/*
 * Process Object Operator Resource Definitions
 *
 * This is the open api definition of the Process Object Operator Resource Definition. 
 *
 * API version: 0.0.1
 * Contact: tylerfrench2@gmail.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi
// ProcessObjFilters struct for ProcessObjFilters
type ProcessObjFilters struct {
	ProcessObjType string `json:"processObjType,omitempty"`
	ProcessObjId string `json:"processObjId,omitempty"`
	ProcessObjPubId string `json:"processObjPubId,omitempty"`
	ProcessObjDisplayName string `json:"processObjDisplayName,omitempty"`
}
