# ProcessObjFilters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProcessObjType** | **string** |  | [optional] 
**ProcessObjId** | **string** |  | [optional] 
**ProcessObjPubId** | **string** |  | [optional] 
**ProcessObjDisplayName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


