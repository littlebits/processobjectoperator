# \ProcessObjectApi

All URIs are relative to *https://localhost:6443/toasters/ProcessObjectResourceDefinition/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateProcessObj**](ProcessObjectApi.md#CreateProcessObj) | **Post** /processobject | Create new Process Object definition
[**DeleteProcessObjById**](ProcessObjectApi.md#DeleteProcessObjById) | **Delete** /processobject/{processObjId} | Delete Specific Process Object definition
[**DeleteProcessObjRecById**](ProcessObjectApi.md#DeleteProcessObjRecById) | **Delete** /processobject/{processObjId}/{processObjRecId} | Delete specific process object record
[**GetProcessObj**](ProcessObjectApi.md#GetProcessObj) | **Get** /processobject | Get Specific Process Object definition
[**UpdateProcessObjDefById**](ProcessObjectApi.md#UpdateProcessObjDefById) | **Post** /processobject/{processObjId} | Update Specific Process Object definition
[**UpdateProcessObjRecById**](ProcessObjectApi.md#UpdateProcessObjRecById) | **Post** /processobject/{processObjId}/{processObjRecId} | Update process object record



## CreateProcessObj

> ProcessObject CreateProcessObj(ctx, optional)

Create new Process Object definition

Create a new Process Object

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***CreateProcessObjOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateProcessObjOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **processObject** | [**optional.Interface of ProcessObject**](ProcessObject.md)|  | 

### Return type

[**ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteProcessObjById

> ProcessObject DeleteProcessObjById(ctx, processObjId)

Delete Specific Process Object definition

Delete specific process object if not records exist

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of the process object | 

### Return type

[**ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteProcessObjRecById

> ProcessObject DeleteProcessObjRecById(ctx, processObjId, processObjRecId)

Delete specific process object record

Delete specific process object record

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of process object | 
**processObjRecId** | **string**| Record id of the process object | 

### Return type

[**ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProcessObj

> ProcessObject GetProcessObj(ctx, processObjId, optional)

Get Specific Process Object definition

Get a process object

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| Id of the process object | 
 **optional** | ***GetProcessObjOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetProcessObjOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **filters** | [**optional.Interface of Filters**](.md)| Filters for updating select process object records, based on fields | 

### Return type

[**ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateProcessObjDefById

> []ProcessObject UpdateProcessObjDefById(ctx, processObjId, optional)

Update Specific Process Object definition

This is the definition required to create a process object.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of process object | 
 **optional** | ***UpdateProcessObjDefByIdOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateProcessObjDefByIdOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **processObject** | [**optional.Interface of ProcessObject**](ProcessObject.md)|  | 

### Return type

[**[]ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateProcessObjRecById

> ProcessObject UpdateProcessObjRecById(ctx, processObjId, processObjRecId, optional)

Update process object record

Update a specific process object record

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of process object | 
**processObjRecId** | **string**| Record ID of process object | 
 **optional** | ***UpdateProcessObjRecByIdOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateProcessObjRecByIdOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **mapStringinterface** | [**optional.Interface of []map[string]interface{}**](map[string]interface{}.md)|  | 

### Return type

[**ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

