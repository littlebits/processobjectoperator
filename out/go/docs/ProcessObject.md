# ProcessObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProcessObjDisplayName** | **string** |  | [optional] 
**ProcessObjType** | **string** |  | 
**ProcessObjId** | **string** |  | 
**ProcessObjName** | **string** |  | [optional] 
**ProcessObjRecId** | **string** |  | [optional] 
**ProcessObjPubId** | **string** |  | [optional] 
**Fields** | [**[]ProcessObjectFields**](ProcessObject_fields.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


