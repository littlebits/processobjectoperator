# \BulkOperationsApi

All URIs are relative to *https://localhost:6443/toasters/ProcessObjectResourceDefinition/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ProcessobjectsGet**](BulkOperationsApi.md#ProcessobjectsGet) | **Get** /processobjects | Get all process object definitions
[**ProcessobjectsProcessObjIdDelete**](BulkOperationsApi.md#ProcessobjectsProcessObjIdDelete) | **Delete** /processobjects/{processObjId} | Delete process object records
[**ProcessobjectsProcessObjIdGet**](BulkOperationsApi.md#ProcessobjectsProcessObjIdGet) | **Get** /processobjects/{processObjId} | Get all process object records
[**UpdateProcessObjById**](BulkOperationsApi.md#UpdateProcessObjById) | **Post** /processobjects/{processObjId} | Update all process object records



## ProcessobjectsGet

> []ProcessObject ProcessobjectsGet(ctx, optional)

Get all process object definitions

These are all the process objects found

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ProcessobjectsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ProcessobjectsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **processObjId** | **optional.String**| Id of process object to run filterd query | 
 **processObjFilters** | [**optional.Interface of ProcessObjFilters**](.md)| Filters for updating select process object records, process object properties | 

### Return type

[**[]ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ProcessobjectsProcessObjIdDelete

> []map[string]interface{} ProcessobjectsProcessObjIdDelete(ctx, processObjId, optional)

Delete process object records

Deletes all records of process object

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of process object | 
 **optional** | ***ProcessobjectsProcessObjIdDeleteOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ProcessobjectsProcessObjIdDeleteOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **processObjFilters** | [**optional.Interface of ProcessObjFilters3**](.md)| Filters for deleting select process object records, based on fields | 

### Return type

[**[]map[string]interface{}**](map[string]interface{}.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ProcessobjectsProcessObjIdGet

> []ProcessObject ProcessobjectsProcessObjIdGet(ctx, processObjId, optional)

Get all process object records

These are all the process object records found

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| Id of the process object | 
 **optional** | ***ProcessobjectsProcessObjIdGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ProcessobjectsProcessObjIdGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **processObjFilters** | [**optional.Interface of ProcessObjFilters1**](.md)| Filters for updating select process object records, based on fields | 

### Return type

[**[]ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateProcessObjById

> []ProcessObject UpdateProcessObjById(ctx, processObjId, optional)

Update all process object records

Update all future base records of a process object

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**processObjId** | **string**| ID of process object | 
 **optional** | ***UpdateProcessObjByIdOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateProcessObjByIdOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **processObjFilters** | [**optional.Interface of ProcessObjFilters2**](.md)| Filters for updating select process object records, based on fields | 
 **processObject** | [**optional.Interface of []ProcessObject**](ProcessObject.md)|  | 

### Return type

[**[]ProcessObject**](ProcessObject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

