# ProcessObjectFields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FieldType** | **string** |  | 
**DisplayName** | **string** |  | [optional] 
**FieldId** | **string** |  | 
**Url** | **string** |  | [optional] 
**InternalName** | **string** |  | [optional] 
**Value** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


