# Object definitions

- Contracts message queue object
```
{
	"contracts": [
		{
			"servicecatalogid": "11234123asd",
			"customerid": "1234123asd",
			"expiration_date": 1573423740, 
			"service_deployments": [
				{
					"service_type": "intel",
					"service_location": "hnd",
					"service_subtype": "intel-compute",
					"service_unit": "vcpu",
					"service_quantity": 100, # 0 == utility, int(64) == subscribed
				},
				...
			]
		},
		...
	]
}
```

- Service Deployment business object
```
{
	"service_type": "intel",
	"service_location": "hnd",
	"service_subtype": "intel-compute",
	"service_unit": "vcpu",
	"service_quantity": 100, # 0 == utility, int(64) == subscribed
	"service_uuid": "1234asdf-asdf1234", # Populated after deployment activities
	"servicecatalogid": "11234123asd",
	"customerid": "1234123asd",
	"expiration_date": 1573423740,
	"metrics": [
		{
			"service_subtype": "intel-compute",
			"service_unit": "vcpu",
			"quantity_metered": 10,
			"service_uuid": "1234asdf-asdf1234",
			"state_id": "asdf1234"
		},
		...
	]
}
```

- Metric summary business object
```
{
	"service_subtype": "intel-compute",
	"service_unit": "vcpu",
	"quantity_metered": 10,
	"service_uuid": "1234asdf-asdf1234"
	"state_id": "asdf1234"
}
```

- Metric object
```
{
	"service_subtype": "intel-compute",
	"service_unit": "vcpu",
	"quantity_metered": 10,
	"service_uuid": "1234asdf-asdf1234"
	"state_id": "asdf1234"
}
```

- Metrics object
```
{
	"data": [
		{
			"service_subtype": "intel-compute",
			"service_unit": "vcpu",
			"quantity_metered": 10,
			"service_uuid": "1234asdf-asdf1234"
			"state_id": "asdf1234"
		},
		...
	]
}
```

- Metrics Table Business Object
```
{
	"intel-hnd-intel-compute": [
		"ReportIntelCpuState(deployment.service_uuid)",
		"ReportIntelMemState(deployment.service_uuid)",
		...
	],
	...
}
```

- Service State business Object
```
{
	"timestamp": 1573424242, # auto populated
	"state_id": "asdf1234", # auto populated
	"service_type": "intel",
	"service_subtype": "intel-compute",
	"metrics": [
		{
			"service_subtype": "intel-compute",
			"service_unit": "vcpu",
			"quantity_metered": 10,
			"service_uuid": "1234asdf-asdf1234"
		},
		...
	]
}
```

# Automation Object definitions

- Service State Object
```
{
	"timestamp": 1573424242,
	"state_id": "asdf1234", 
	"service_type": "intel",
	"service_subtype": "intel-compute",
	"metrics": [
		{
			"service_subtype": "intel-compute",
			"service_unit": "vcpu",
			"quantity_metered": 10,
			"service_uuid": "1234asdf-asdf1234"
		},
		...
	]
}
```

# Object Functions

- Contracts parser ()
```
if len(`contract.contracts`) > 0:
	for each `contract` in `contract.contracts`:
		for each `service_deployment` in `contract.service_deployments`:
			get service deployment business object, `template`
			fill in `template` values where `service_deployment.key` == `template.key`
			fill in `contract.contractmappingid`, `contract.customerid`
```

- Metrics parser ()
```
Get all service deployment business objects, `deployments`
for each `deployment` in `deployments`:
	new metrics object, `metrics`
	Get active service types, `active_types`
	for each `type` in `active_types`:
		if `type.name` == `deployment.service_type` && `type.subtype` == `deployment.service_subtype`:
			new metrics object, `metrics`
			type.MetricsGenerator(type.name, type.subtype, deployment.location, deployment.service_uuid, &metrics)
			metrics.data.append(result, metrics.data)
	deployment.metrics.append(metrics, deployment.metrics)
```

- Metrics Generator (type.name, type.subtype, deployment.location, deployment.service_uuid, &metrics)
```
Get metrics table business objects, `metric_table` 
for each `report` in `metric_table["{0}-{1}-{2}"]`.(type.name, deployment.location, type.subtype):
	New metric object, `metric`
	metric = report.key(deployment.service_uuid)
	&metrics.append(metric, &metrics.data)
```

# Automation Operations 

- State Generator ()
```
Get list of active services, `services`
for each `service` in `services`:
	Get instances of service, `instances`
	for each `instance` in `instances`:
		Get service state business object, `template`
		Gather state for all `tenants` of `instance`, `state`
		fill in `template` values where `template.type` and `template.sub_type` == `service.type` and `service.sub_type`
```