# How to build new go server based off api def

```bash
$> docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate -i /local/apidef.yaml -g go -o /local/out/go
```

### Cors

This is the best example of cors, its basically security policy https://en.wikipedia.org/wiki/Cross-origin_resource_sharing


## Tasks

* TODO: Research idead of building it multitenant from the start: https://github.com/coreos/etcd-operator
* TODO: Write documentation on how to use DNS SRV records to handle discovery https://github.com/etcd-io/etcd/blob/master/Documentation/v2/clustering.md#dns-discovery
* TODO: Research how rbac works in etcd 
* TODO: Add to generated go the etcd client integration to store the data: https://github.com/etcd-io/etcd/tree/master/client
* TODO: Write terraform to deploy:
	- coreos etcd cluster: https://coreos.com/os/docs/latest/cluster-discovery.html
		- variable sizes
		- DNS SRV record discovery
	- firewall rules(security group) for etcd communication:
		- 2380
		- 2379
		- 443
