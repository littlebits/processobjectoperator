provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

data "aws_route53_zone" "base" {
  name = var.base_domain
  private_zone = true
}

resource "aws_route53_zone" "cluster" {
  name          = var.cluster_domain
  force_destroy = true

  tags = map(
    "Name", var.cluster_domain,
    "Platform", "vCD"
    )
}

resource "aws_route53_record" "etcd_a_nodes" {
  count   = var.etcd_nodes

  type    = "A"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "etcd-${count.index}.${var.cluster_domain}"
  records = ["${element(var.etcd_nodes, count.index)}"]
}

resource "aws_route53_record" "etcd_cluster" {
  type    = "SRV"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "_etcd-server-ssl._tcp"
  records = formatlist("0 10 2380 %s", aws_route53_record.etcd_a_nodes.*.fqdn)
}