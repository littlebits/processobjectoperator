variable "cluster_domain" {
  type        = string
  description = "The domain for the cluster that all DNS records must belong"
}

variable "base_domain" {
  type        = string
  description = "The base domain used for public records."
}

variable "etcd_nodes" {
  type = list
  description = "List of etcd nodes to create records for."
}

variable "aws_region" {
  type = string
  description = "AWS region for deployment."
}

variable "aws_access_key" {
  type = string
  description = "AWS access key"
}

variable "aws_secret_key" {
  type = string
  description = "AWS secret key"
}