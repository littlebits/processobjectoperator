# Business Object Operator standard object

This is an operator that handles the, _safe_ creation of a busiiness object (CRD) which is a wrapper around the `kubectl create -f business_object.yaml` basically. It also handles the _safe_ version of all the other _CRUD_ type operations that might need to happen to a business object.

## Standard

```yaml
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  # name must match the spec fields below, and be in the form: <plural>.<group>
  name: processObject.stable.frenchtoastman.com
spec:
  # group name to use for REST API: /apis/<group>/<version>
  group: stable.frenchtoastman.com
  # list of versions supported by this CustomResourceDefinition
  versions:
    - name: v1
      # Each version can be enabled/disabled by Served flag.
      served: true
      # One and only one version must be marked as the storage version.
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          properties:
            ProcessObject:
              type: object
              properties:
                processObjDisplayName:
                  type: string
                processObjType:
                  type: string
                processObjId:
                  type: string
                processObjName:
                  type: string
                processObjRecId:
                  type: string
                processObjPubId:
                  type: string
                fields: 
                  type: array
                  items:
                    type: object
                    properties:
                      fieldType:
                        type: string
                      displayName:
                        type: string
                      fieldId:
                        type: string
                      url:
                        type: string
                      internalName:
                        type: string
                      value:
                        type: string
                    required:
                      - fieldType
                      - fieldId
                      - value
              required: 
              - "processObjId"
              - "processObjType"
  # either Namespaced or Cluster
  scope: Namespaced
  names:
    # plural name to be used in the URL: /apis/<group>/<version>/<plural>
    plural: processobjects
    # singular name to be used as an alias on the CLI and for display
    singular: procesobject
    # kind is normally the CamelCased singular type. Your resource manifests use this.
    kind: ProcessObject
    # shortNames allow shorter string to match your resource on the CLI
    shortNames:
    - po  
```

## Queries using kubectl

* Display all ProcessObjects
```
$> kubectl get processobjects.stable.frenchtoastman.com first-process-object -o custom-columns=NAME:.spec.ProcessObject.processObjDisplayName
```

***NOTE*** all other queries are going to map to kubectl commands
***NOTE*** multitenancy is provided through namespace/rbac restricitions
***NOTE*** labels utilized to denote version control and status
***NOTE*** all KPI metric gather can be done via prometheus queries for analytics
